| 对比项     | cboard                                                     | dataease                                               | 个人看法                                         |
|------------|------------------------------------------------------------|--------------------------------------------------------|--------------------------------------------------|
| github星   | 2.8k                                                       | 5.5k                                                   | dataease热度更高些                               |
| 架构       | jsp+springmvc+Echarts                                      | springboot+vue+Elementui                               | dataease架构更符合当前技术趋势                   |
| 易用性     | 差                                                         | 好                                                     | dataease用户操作性上更优                         |
| 数据源支持 | JDBC协议数据库产品，elasticsearch,kylin,离线文件，json文本 | excel,doris,ClickHouse                                 | dataease需要学习Apache doris、kettle             |
| 分析引擎   | 自研                                                       | Doris及Kettle                                          | dataease使用相对热门开源数据处理技术             |
| 学习成本   | 高                                                         | 高                                                     | 在学习成本上两者是一样的                       |
| 更新频率   | 低                                                         | 高                                                     | dataease每月更新一次                             |
| 文档质量   | 低                                                         | 高                                                     | cboard的文档较dataease少了许多，没有官方用户手册 |
| 协议       | apache-license2.0                                          | GPL3.0                                                 | cboard允许商用、二次开发，这点是较dataease占优的 |
| 其他       | cboard因年代久些也有Vue相应的UI及教程，二次开发友好些      | dataease相当于提供了产品级的可视化工具，基本不需要二次开发 | 交互性dataease占优，技术角度讲cboard占优,可用dataease学习及过渡用,若要形成自己的产品建议学习cboard为主         |
